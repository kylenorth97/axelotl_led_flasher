// led_control.c
// Runs on LM4F120/TM4C123
// Kyle North
// Firmware developed for senior design and PNNL to flash 
// an LED via bluetooth inside of our light tight box
// 2/24/2020

// input signal connected to PE2/AIN1

#include "led_control.h"
#include "tm4c123gh6pm.h"
#include "PLL.h"
#include "UART.h"
#include "stdint.h"
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <stdbool.h>


// 2. Declarations Section
//   Global Variables
unsigned long In;
unsigned long Tick; 
int delay = 0;
int flash = 0;

//   Function Prototypes
void PortEInit(void);
void PortFInit(void);
void SysTick_Wait(uint32_t delay);
void SysTick_Wait10ms(uint32_t delay);
void SysTick_Init(unsigned long period);
void EnableInterrupts(void);
void DisableInterrupts(void); // Disable interrupts

//---------------------OutCRLF---------------------
// Output a CR,LF to UART to go to a new line
// Input: none
// Output: none

void OutCRLF(void){
  UART_OutChar(CR);
}
int main(void){unsigned long volatile delay;
  PLL_Init();                           // 80 MHz
	UART_Init();
	PortEInit();
	PortFInit();
	SysTick_Init(80000000);
	EnableInterrupts();
  SYSCTL_RCGC2_R |= SYSCTL_RCGC2_GPIOF; // activate port F
  delay = SYSCTL_RCGC2_R;
	
	GPIO_PORTF_DATA_R = 0x00;    // red led off 
  while(1){
    UART_OutString("0:off, 1:on, 2:flash: ");
    In = UART_InUDec();
		OutCRLF();
		if(In == 0) // LED off
		{
			UART_OutString("off\n");
			GPIO_PORTE_DATA_R &= ~0x08;		 // LED off 
			flash = 0;
		}
			
		if(In == 1) // LED on
		{
			UART_OutString("on\n");
			GPIO_PORTE_DATA_R |= 0x08;     // LED on
			flash = 0;
		}
		
		if(In == 2) // LED flash
		{
			UART_OutString("flash\n");
			flash = 1;
		}
	
		OutCRLF();
		}
}

void PortEInit(void) {
    volatile unsigned long delay;
 
    SYSCTL_RCGC2_R    |= 0x00000010;     // 1) E clock
    delay              = SYSCTL_RCGC2_R; // delay
    //GPIO_PORTE_LOCK_R  = 0x4C4F434B;     // 2) unlock PortE
    GPIO_PORTE_CR_R   |= 0x1F;           // allow changes to PE1, PE0
    GPIO_PORTE_AMSEL_R = 0x00;           // 3) disable analog function
    GPIO_PORTE_PCTL_R  = 0x00;           // 4) GPIO clear bit PCTL
    GPIO_PORTE_DIR_R  |= 0x0E;           // 5) PE1 input, PE0 output
    GPIO_PORTE_AFSEL_R = 0x00;           // 6) no alternate function
    GPIO_PORTE_DEN_R  |= 0x1F;           // 7) enable digital pins PE1, PE0
		GPIO_PORTE_PUR_R = 0x00;          // enable pull-up       
}

void SysTick_Init(unsigned long period){
 // Setup Routine
	//SYSCTL_RCC_R |= 0x15; // clock at 16MHz
	//SYSCTL_RCC_R |= 0x2000000; // second clock divider
  NVIC_ST_CTRL_R = 0;         // (a) disable SysTick during setup
  NVIC_ST_RELOAD_R = period-1;// (b) reload value
  NVIC_ST_CURRENT_R = 0;      // (c) any write to current clears it
  NVIC_SYS_PRI3_R = (NVIC_SYS_PRI3_R&0x00FFFFFF)|0x40000000; // (d) priority 2
 
  // Re-enable SysTick & Interupts
  NVIC_ST_CTRL_R = 0x07;      // (e) enable SysTick with core clock and interrupts
  EnableInterrupts();         // (f) clear I bit
} 

void SysTick_Handler(void){
	Tick++;
	if(flash)
		GPIO_PORTE_DATA_R ^= 0x08;     // LED on
}

void PortFInit(void){ volatile unsigned long delay;
  SYSCTL_RCGC2_R |= 0x00000020;     // 1) activate clock for Port F
  delay = SYSCTL_RCGC2_R;           // allow time for clock to start
  GPIO_PORTF_LOCK_R = 0x4C4F434B;   // 2) unlock GPIO Port F
  GPIO_PORTF_CR_R = 0x1F;           // allow changes to PF4-0
  // only PF0 needs to be unlocked, other bits can't be locked
  GPIO_PORTF_AMSEL_R = 0x00;        // 3) disable analog on PF
  GPIO_PORTF_PCTL_R = 0x00000000;   // 4) PCTL GPIO on PF4-0
  GPIO_PORTF_DIR_R = 0x0E;          // 5) PF4,PF0 in, PF3-1 out
  GPIO_PORTF_AFSEL_R = 0x00;        // 6) disable alt funct on PF7-0
  GPIO_PORTF_PUR_R = 0x11;          // enable pull-up on PF0 and PF4
  GPIO_PORTF_DEN_R = 0x1F;          // 7) enable digital I/O on PF4-0
}

// Color    LED(s) PortF
// dark     ---    0
// red      R--    0x02
// blue     --B    0x04
// green    -G-    0x08
// yellow   RG-    0x0A
// sky blue -GB    0x0C
// white    RGB    0x0E
// pink     R-B    0x06

// Subroutine to wait 0.1 sec
// Inputs: None
// Outputs: None
// Notes: ...

// The delay parameter is in units of the 80 MHz core clock(12.5 ns) 
void SysTick_Wait(uint32_t delay){
  NVIC_ST_RELOAD_R = delay-1;  // number of counts
  NVIC_ST_CURRENT_R = 0;       // any value written to CURRENT clears
	Tick = 0;	
	while(!Tick);
}

// Call this routine to wait for delay*10ms
void SysTick_Wait10ms(uint32_t delay){
	 unsigned long i; 
	 for(i=0; i<delay; i++){
		 SysTick_Wait(800000); // wait 10ms
	}
}


